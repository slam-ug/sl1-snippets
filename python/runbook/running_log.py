# Since EM7_RESULT isn't always a good way to see what's going on, you can do something like:
import silo_common.snippets as em7_snippets

DEBUG = True  # Change to False to disable logging

if DEBUG:
    logs = em7_snippets.logger(file='/var/log/em7/my_rba.log')
if not DEBUG:
    logs = em7_snippets.logger(file='/dev/null')

logs.debug("Debug-level message")
logs.error("Error-level message")
# And so on...it uses the Python logger values so you can adjust severity based on your code.
# Anywhere you'd print or use EM7_RESULT you can use logs instead.

# To view logs as you test your snippets, run this on the CLI:
# tail -f /var/log/em7/my_rba.log
# to see a real-time update of the data in your RBA.
