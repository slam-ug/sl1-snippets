# before we can connect to a specific powershell URI (rather than the default one), we need to embed that login in our powershell script.
from silo_common.snippets.powershell import powershell_winrm
import socket,code,re
from collections import namedtuple


#Get the ip address or hostname in the credential. SL1 replaces %D or %N with an actual value
match = re.match(r'(?i)(?:\w+://)?(?P<srv>.+?)$',self.cred_details['cred_host'])

# Using reverse dns lookup, find the dns servername
revdns = socket.gethostbyaddr(match.group('srv'))

# Get the fqdn from the reverse result
dst = revdns[0]

# find out what has been set: encrypted or unencrypted communication to the host
if self.cred_details['ps_encrypted'] == 1:
    proto = 'https'
else:
    proto = 'http'

# Build the uri to connect to. In this case, I connect (again) to the WSMAN, but before doing that 2nd connect, we specify a different configuration
uri = '{}://{}:{}/wsman'.format(proto, dst, self.cred_details['cred_port'])

if self.cred_details['ps_account_type'] == 1: # AD account
    username = '{}\\{}'.format(self.cred_details['ps_ad_domain'], self.cred_details['cred_user'])
else:
    username = self.cred_details['cred_user']

script = '' # Replace this with the contents of compagnion file 'Example-pssessionconfiguration.ps1' or by whatever you want to run on the destination server.

#build the complete powershell request.
ps = '''$password = ConvertTo-SecureString -String '{pwd}' -AsPlainText -Force
$credential = New-Object -TypeName pscredential -ArgumentList @('{username}',$password)
$session_option = New-PSSessionOption -SkipCACheck -SkipCNCheck -SkipRevocationCheck -Culture ([cultureinfo]'en-us')
$session = New-PSSession -ConfigurationName '{endpoint}' -ConnectionUri '{uri}' -Credential $credential -SessionOption $session_option -Authentication Kerberos
$Output = Invoke-Command -Session $session -ScriptBlock {{ {script} }}
write-output ($Output | fl | Out-String)'''.format(
    pwd=self.cred_details['cred_pwd'],
    username= username,
    uri=uri,
    script=script,
    endpoint='microsoft.exchange')

Request = namedtuple('Request', 'req_id key_column request app_id')

# make sure the second parameter contains an attribute that is returned by the powershell script, 'keyfield' in my case.
request = Request(-1, 'keyfield', ps, self.app_id) 

# do the actual request. this will first connect to the server and then execute the script, which in turn will create a second connection to the specific endpoint
psdata, pserror = powershell_winrm(self.did, self.ip, request, self.cred_details, True, None, None, self.logger)

# the rest is the same as the other example.