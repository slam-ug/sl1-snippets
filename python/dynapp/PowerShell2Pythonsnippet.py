#Imagine a python dynapp requesting the following collecton objects:
# disklabel
# diskcapacity
# diskfreespace

# We first initialize the environment. We also use variable 'results' to temporary store everything we want to send to ScienceLogic

from collections import namedtuple
Request = namedtuple('Request', 'req_id key_column request app_id')

results= {}
for reskey in result_handler.iterkeys():
  results[reskey] = []

# then, the powershell script. You can find a copy with comments in the compagnion file GetDiskSize.ps1
poshrequest = r'''$Disks = Get-PSDrive -name [CD]
if ($Disks) {
    $Results = @()
    foreach ($Disk in $Disks) {
        $Results += New-Object -TypeName psobject -property @{
            keyfield = $Disk.Name
            capacity = $Disk.Used + $Disk.free
            freespace = $Disk.free
        }
    }
}
Write-Output ($Results | Select-Object keyfield, * -ErrorAction SilentlyContinue | format-list | Out-String )'''

# We can invoke the powershell script like this
request = Request(-1, 'keyfield', poshrequest , self.app_id)
poshok, posherror = powershell_winrm(self.did, root_device.ip, request, self.cred_details, True, None, None, self.logger)

# Assuming no credential or other errors and a disk with 2 drives, this is the expected result

posh= {'keyfield':{'c:':'c:','d:':'d:'}, 'capacity':{'c:','4000','d:','15000'}, 'freespace':{'c:':'3000','d:':'13999'}}
posherror=''

# This is how we send it all back
for key in posh['keyfield']:
  results['disklabel'].append( (key,posh['keyfield'][key]) )  #MIND the double (()) !! SL requires a tuple to be returned!
  results['disksize'].append( (key, posh['capacity'][key]) )
  results['diskfree'].append( (key, posh['freespace'][key]))

result_handler.update(results)