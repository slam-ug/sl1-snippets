# Python Dynamic Applications Snippets

Directory containing useful scripts for dynamic applications.

## Snippets

| Example name | Description | required files |
|--------------|-------------|----------------|
| PowerShell2Pythonsnippet.py | This example shows how to use the function 'powershell_winrm' to connect from a SL1 collector to a windows server using powershell | * PowerShell2Pythonsnippet.py<br />* GetDiskSize.ps1 |
| Example-configurationuri.py | This example continues on the previous example, but creates a second session to a specific configuration-uri or name.<br />Microsoft Exchange for example is only accessible via its own connectionuri | * Example-configurationuri.py<br />* Example-pssessionconfiguration.ps1 |