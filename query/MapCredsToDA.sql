#This snippet will list all the DA's aligned to devices where the DA is not using the default credentials
#You can extent the WHERE statement by adding "AND mmdadc.cred_id='1234'" to look for a specific credential
#or you can add "AND mdld.did='1234'" for the did of a particular device
#or finally you could "AND mmdadc.app_id='1234'" to find all the non-standard use of an DA
#
#Rabie van der Merwe
#2019/08/10
#
SELECT mmdadc.did, mdld.device, mmdadc.app_id, mda.name, mmdadc.cred_id, msc.cred_name 
FROM master.map_dynamic_app_device_cred AS mmdadc 
LEFT JOIN master_dev.legend_device mdld ON mmdadc.did=mdld.id 
LEFT JOIN master.dynamic_app mda ON mmdadc.app_id=mda.aid 
LEFT JOIN master.system_credentials msc ON mmdadc.cred_id=msc.cred_id 
WHERE mmdadc.cred_id!='0';