# Device Credentials

## Summary

SL1 uses Device Credentials to authenticate to devices. Those credentials are stored in the database.
No worries though: the passwords are not in clear text visible, but are hashed by the system.

## What you need to know upfront

- Credentials need to be aligned to at lease one (or all) organizations. The query to [get the links between a credential and an organization](./credential_organization.sql) will show you which credentials are NOT aligned to all organizations. roa_id refers to the field master_biz.organizations.roa_id
- A credential must also be a specific type. The [list of credential types](./credential_types.sql) query will list you which types are available.
- Database type credentials also have a [specific database type](./db_credential_types.sql)

## Additional properties are stored in different tables

Before we can actually get the credential information, we need to know which table holds more information for a specific credential type:

- Basic/Snippet are the most basic types of credentials and have no extra properties
- [Database properties](./db_credential_properties.sql) credential types have several extra properties
- [LDAP/AD properties](./ldap_credential_properties.sql)
- [PowerShell properties](./winrm_credential_properties.sql)
- [SNMP properties](./snmp_credential_properties.sql)
- [SOAP/XML properties](./soap_credential_properties.sql)
- [SSH properties](./ssh_credential_properties.sql)

## yes, NOW we bring it all together

The query for [basic/snippet credentials](./credentials.sql) can now be used to list all basic properties for any credential.<br />
[Database credential](./db_credentials.sql) lists all information about database-type credentials<br />
[LDAP credential](./ldap_credentials.sql) lists all information about ldap-type credentials<br />
[PowerShell credential](./winrm_credentials.sql) lists all information about PowerShell-type credentials<br />
[SNMP credential](./snmp_credentials.sql) lists all information about SNMP-type credentials<br />
[SOAP/XML credential](./soap_credentials.sql) lists all information about SOAP\/XML type credentials<br />
[SSH credential](./ssh_credentials.sql) lists all information about all ssh-type credential. Since SSH can contain PEM files, they are stored in a blob and are quite unreadable.
