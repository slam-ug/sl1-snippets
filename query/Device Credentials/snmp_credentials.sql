select sc.cred_id, sc.cred_name, sc.cred_user
    #, sc.cred_pwd # is commented out because it''s value is hashed
    , sc.cred_host, sc.cred_port
    , dct.name as credential_type_name
    , sc.cred_timeout
    , if(sc.all_orgs=1,'all organizations', maco.orgs )
    , mscs.cred_id
    , mscs.snmp_version
    # , mscs.snmp_ro_community # hidden because it is hashed
    # , mscs.snmp_rw_community # hidden because it is hashed
    , mscs.snmp_timeout
    , mscs.snmp_retries
    , mscs.snmpv3_auth_proto
    , mscs.snmpv3_sec_level
    , mscs.snmpv3_priv_proto
    #, mscs.snmpv3_priv_pwd # hidden because it is hashed
    , mscs.snmpv3_context
    , mscs.snmpv3_engine_id
from master.system_credentials as sc
inner join master.definitions_credential_types dct on sc.cred_type=dct.cred_type
inner join master.system_credentials_snmp as mscs on mscs.cred_id = sc.cred_id
left join (
    select co.cred_id, group_concat(o.company separator ',') as orgs
    from master_access.credentials_organizations as co
    inner join master_biz.organizations as o on co.roa_id = o.roa_id
    group by cred_id
) as maco on sc.cred_id = maco.cred_id