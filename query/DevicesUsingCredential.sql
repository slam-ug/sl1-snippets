# Get list of DIDs using a specific credential
SELECT GROUP_CONCAT(DISTINCT(did))
FROM master.map_dynamic_app_device_cred
WHERE cred_id = <CRED_ID>;