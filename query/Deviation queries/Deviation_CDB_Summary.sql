SELECT col.did, ld.pid, obj.app_id, app.name as 'Application Name', obj.name as 'Collection Object name'
, obj.obj_id, obj.class, obj.std_window, obj.std_min, daim.ind, daim.instance, 
IFNULL(dasm.newest_collection_time, '0000-00-00 00:00:00') AS newest_collection_time, 
IFNULL(dasm.oldest_collection_time, '9999-12-31 23:59:59') AS oldest_collection_time 
FROM master.dynamic_app_objects obj 
inner join master.dynamic_app as app on app.app_guid = obj.app_guid
INNER JOIN master.dynamic_app_collection col ON (obj.obj_id = col.Z_id) 
INNER JOIN master_dev.legend_device ld ON (col.did = ld.id) 
INNER JOIN master.dynamic_app_index_map daim ON daim.did = col.did AND daim.app_id = obj.app_id AND daim.group_id = obj.array_group 
LEFT JOIN master.dynamic_app_std_meta dasm ON col.did = dasm.did AND ld.pid = dasm.pid AND obj.obj_id = dasm.obj_id AND daim.instance = dasm.instance 
WHERE obj.std_enable = 1