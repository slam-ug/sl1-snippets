# Source: ScienceLogic Support
# I believe this should list all root devices and how many component devices they have in a particular org.  If you see any root device listed twice, it has components in different orgs.  It worked in my lab, where I deliberately changed a component device to a different org.
SELECT cdm.root_did,
        ld.roa_id,
        count(cdm.id)
FROM master_dev.component_dev_map cdm
JOIN master_dev.legend_device ld
    ON ld.id = cdm.component_did
WHERE root_did IN 
    (SELECT DISTINCT root_did
    FROM master_dev.component_dev_map)
GROUP BY  cdm.root_did,ld.roa_id