# Source: ScienceLogic Support
# Below is a query that can help search your entire DB for component devices that contain a specific DA.
SELECT a.did,
         a.name,
         a.ip,
         a.cmp_dev,
         b.app_id
FROM collector_state.V_device a
JOIN master.dynamic_app_collection b
    ON a.did=b.did
WHERE a.cmp_dev = 1
        AND b.app_id = <APP_ID> limit 100