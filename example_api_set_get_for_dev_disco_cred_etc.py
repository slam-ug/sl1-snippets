import argparse
import json
import sys
from re import split

import requests

__version__ = "1.0.0"

requests.packages.urllib3.disable_warnings()
# OpsVision test box
BASE_URL_PREFIX = "https://student"
BASE_URL_DOMAIN = ".test.com"
HEADERS = {"Content-Type": "application/json"}
CO = "Math Matrix Learning"
PREFILTER = "?limit=10&hide_filterinfo=1&"
UID = "userid"
PWD = "p@ssword"
DISCO_START = '1.1.1.1'
DISCO_END = '1.1.1.9'


# check for the existence of the MM org, setup if doesn't exist.  Exit if more than one.
def set_org(student_url):
    # CO = 'Math Matrix Learning'
    org_match = []
    filter_info = "filter.company={}".format(CO)
    org_uri = "/api/organization"
    get_url = student_url + org_uri + PREFILTER + filter_info
    set_url = student_url + org_uri

    try:
        res = requests.get(get_url, auth=(UID, PWD), headers=HEADERS, verify=False)
        if res.status_code == 200:
            org_match = res.json()
            if org_match:
                print("\tWe found a matching org, no need to create one. Match is: {}".format(org_match[0]['URI']))
                # if more than than one org returns we want to exit and correct it first
                if len(org_match) > 1:
                    print("\tReturned more than one org of the same name.  We don't know which one to use. Exiting.")
                    sys.exit()
            else:
                print("\tNo matching org, null result from API get so we'll create one.")
                org_payload = json.dumps({"updated_by": "/api/account/2", "roa_guid": "", "company": CO, })
                res = requests.post(set_url, auth=(UID, PWD), data=org_payload, headers=HEADERS, verify=False, )
                org_match = res.json()
                if res.status_code != 201:
                    print("\tOrganization not created, Exiting with return code: {}".format(res.status_code))
                    sys.exit()
                print("\tSuccessfully created the Math Matrix Learning (roa_guid: {}) organization.".format(
                    org_match['roa_guid']))
            return org_match
        else:
            print("\t\tGet on organization failed: Return code: {}.  Exiting.".format(res.status_code))
            sys.exit()
    except Exception as e:
        print("\t\tException in set_org(): {}".format(e))
        sys.exit()


def set_cred(student_url):
    filter_info = "filter.cred_name={}".format(CO)
    snmp_uri = "/api/credential/snmp"
    get_url = student_url + snmp_uri + PREFILTER + filter_info
    set_url = student_url + snmp_uri
    try:
        res = requests.get(get_url, auth=(UID, PWD), headers=HEADERS, verify=False)
        if res.status_code == 200:
            cred_match = res.json()
            if cred_match:
                print("\tWe found a matching cred, no need to create one. Match is: {}".format(cred_match[0]['URI']))
                # if more than than one org returns we want to exit and correct it first
                if len(cred_match) > 1:
                    print("\tReturned more than one cred of the same name.  We don't know which one to use. Exiting.")
                    sys.exit()
                return True
            else:
                print("\t\tNo matching cred, null result from API get so we'll create one.")
                cred_payload = json.dumps({'cred_name': CO, 'cred_port': '161', 'snmp_retries': 1, 'snmp_version': 2,
                                           'snmp_ro_community': 'tr@ining', })
                res = requests.post(set_url, auth=(UID, PWD), data=cred_payload, headers=HEADERS, verify=False)
                cred_match = res.json()
                if res.status_code == 201:
                    print("\tSuccessfully created the {} (cred_guid: {}) SNMP credential".format(CO, cred_match[
                        'cred_guid']))
                    return True
                else:
                    print("\t\tSetting the SNMP cred failed with return code: {}".format(res.status_code))
                    sys.exit()
    except Exception as e:
        print("\t\tException on set_cred: {}".format(e))
        sys.exit()


def set_device(student_url):
    dev2org = {'ccservnc1sw1': 1, 'ccservnc1temp1': 1, 'ccservnc1exch1': 2, 'ccservnc1ad1': 2, 'Corp-AD01': 2,
               'ccservnc1ad2': 2, 'robonc1wp1': 4}
    filter_info = "filter.name="
    dev_uri = "/api/device"
    get_url = student_url + dev_uri + PREFILTER + filter_info
    try:
        x = 0
        for device, org in dev2org.items():
            get_url = student_url + dev_uri + PREFILTER + filter_info + device
            res = requests.get(get_url, auth=(UID, PWD), headers=HEADERS, verify=False)
            dev_match = res.json()
            if dev_match:
                if res.status_code == 200:
                    if len(dev_match) > 1:
                        print(
                            "\t\tMore than one device returned for {}, we dont know which to use.  Exiting. \n{}".format(
                                device, dev_match))
                        sys.exit()
                    else:
                        dev_org_payload = json.dumps({"organization": "/api/organization/{}".format(org)})
                        set_url = student_url + dev_match[0]['URI']
                        res = requests.post(set_url, auth=(UID, PWD), data=dev_org_payload, headers=HEADERS,
                                            verify=False)
                        if res.status_code == 200:
                            print("\tSuccess updating device: {} with org {}".format(device, org))
                        else:
                            print("\t\tDevice update failed for {}.  Return code: {}".format(device, res.status_code))
                            print(res.json())
                else:
                    print(get_url)
                    print("\t\tReturn Code: {}".format(res.status_code))
                    sys.exit()
            else:
                print("\t\tNo device match for {}.  Skipping.".format(device))
        # sys.exit()
        x += 1
        return True
    except Exception as e:
        print("\t\tException setting devices: {}".format(e))
        return False


def set_disco(student_url):
    org_uri = '/api/organization'
    org_filter = PREFILTER + "filter.company={}".format(CO)
    org_get = student_url + org_uri + org_filter

    app_uri = '/api/appliance'
    app_filter = "?limit=1&hide_filterinfo=1&"  # it's an AIO so just return the first and only entry
    app_get = student_url + app_uri + app_filter

    cred_uri = '/api/credential/snmp'
    cred_filter = PREFILTER + "filter.cred_name={}".format(CO)
    cred_get = student_url + cred_uri + cred_filter

    disco_uri = '/api/discovery_session'
    disco_filter = "filter.name=Math Matrix"
    set_disco_url = student_url + disco_uri
    set_active_url = student_url + disco_uri + "_active"
    get_disco_url = student_url + disco_uri + PREFILTER + disco_filter
    disco_name = "Math Matrix"
    disco_descript = "Math Matrix application servers"

    # add data collection get urls to a list to iterate through
    disco_gets = [org_get, app_get, cred_get]

    try:
        x = 0
        disco_var_set = []
        # collect org, appliance, and cred values for disco creation from 'disco_gets' list
        for url in disco_gets:
            res = requests.get(url, auth=(UID, PWD), headers=HEADERS, verify=False)
            if res.status_code == 200:
                disco_var_set.append(res.json())
            else:
                print("\t\tRequest set_disco() for org, cred, and/or appliance failed.  Return code: {}".format(
                    res.status_code))
                sys.exit()
            x += 1
        # assign returned URI values to easy to reference variables
        org = disco_var_set[0][0]['URI']
        collector = disco_var_set[1][0]['URI']
        cred = disco_var_set[2][0]['URI']

        res = requests.get(get_disco_url, auth=(UID, PWD), headers=HEADERS, verify=False)
        if res.status_code == 200:
            if res.json():
                print("\tA discovery named Math Matrix appears to already exist, skip creation and keep going.")
                return True
        else:
            print("\t\tAttempt to query for an existing discovery named Math Matrix failed.  Return code: {}".format(
                res.status_code))
            sys.exit()

        disco_payload = json.dumps({
            "organization": org,
            "aligned_collector": collector,
            "port_scan_timeout": "",
            "initial_scan_level": "",
            "scan_throttle": "",
            "name": disco_name,
            "description": disco_descript,
            "ip_lists": [{'start_ip': DISCO_START, 'end_ip': DISCO_END}],
            "credentials": [cred]})

        # create the discovery
        res = requests.post(set_active_url, auth=(UID, PWD), data=disco_payload, headers=HEADERS, verify=False)
        if res.status_code == 201:
            print("\tDiscovery creation and initiation successful.")

            # query the newly created discovery data to get the index
            res = requests.get(get_disco_url, auth=(UID, PWD), data=disco_payload, headers=HEADERS, verify=False)
            # capture the disco URI info for use when checking if the discovery is running
            disco_match = res.json()
            new_disco_index = split("/", disco_match[0]['URI'])[3]

            # return whatever is in the logs for the newly created discovery
            get_disco_log_url = student_url + disco_uri + "/" + new_disco_index + "/log" + PREFILTER
            disco_active = True
            while disco_active:
                res = requests.get(get_disco_log_url, auth=(UID, PWD), headers=HEADERS, verify=False)
                disco_status = res.json()
                # print("\tDiscovery is running....")
                # sleep(3)
                if len(disco_status) == 2:
                    if disco_status[1]['description'] == "Auto-discovery session completed":
                        print("\t\tDiscovery is complete.")
                        disco_active = False
                    else:
                        print("\t\tDiscovery ended but without a successful completion message.")
                        print(disco_status[1]['description'])
                        sys.exit()
                disco_active = False
            return True
        else:
            print("\t\tDiscovery creation was unsuccessful and returned code: {}".format(res.status_code))
            return False
    except Exception as e:
        print("\t\tException in set_disco(): {}".format(e))
        sys.exit()


# handle set of labs from command line parsing
parser = argparse.ArgumentParser(
    description='ScienceLogic Expert Series student lab vm setup script')
parser.add_argument(
    'start', metavar='begin', type=int, help='Starting lab system, ie student03 would be 3')
parser.add_argument(
    'end', metavar='end', type=int, help='The ending lab system, ie student11 would be 11')
args = parser.parse_args()

# student = FIRST_STUDENT
curr_lab = args.start

while curr_lab <= args.end:
    if curr_lab <= 9:
        stud_with_zero = "0" + str(curr_lab)
        api_url = BASE_URL_PREFIX + stud_with_zero + BASE_URL_DOMAIN
    else:
        api_url = BASE_URL_PREFIX + str(curr_lab) + BASE_URL_DOMAIN
    print("Setting up {} for expert series configuration.".format(api_url))
    print("Attempt to create Math Matrix organization")
    set_org(api_url)
    print("Attempt to create SNMP Credential")
    set_cred(api_url)
    print("Attempt to create and run Discovery Session")
    set_disco(api_url)
    print("Attempt assigning devices into Cloud Compass Infrastructure, Cloud Compass IT, and Robonews.net orgs")
    set_device(api_url)
    curr_lab += 1
    print("Completed without any critical errors.  The end.")
    print("\n")
